function ajaxNotAsync(urls, valor) {
  let dataLocal = '';
  $.ajax({
    url: urls,
    type: 'POST',
    data: valor,
    dataType: 'json',
    async: false,
    success(data) {
      dataLocal = data;
    },
    failure(data) {
      console.log('error ');
    }
  });
  return dataLocal;
}

function getAllCount() {
  let products = ajaxNotAsync('https://consult-wp1.cirel.com.ve/controllers/products-controller.php', {
    type: 'consultAll',
    route: 'products'
  });
  let sales = ajaxNotAsync('https://consult-wp1.cirel.com.ve/controllers/products-controller.php', {
    type: 'consultAll',
    route: 'reports/sales'
  });
  let customers = ajaxNotAsync('https://consult-wp1.cirel.com.ve/controllers/products-controller.php', {
    type: 'consultAll',
    route: 'customers'
  });
  let orders = ajaxNotAsync('https://consult-wp1.cirel.com.ve/controllers/products-controller.php', {
    type: 'consultAll',
    route: 'orders'
  });
  if (products) {
    if (!('name' in products[0])) {
      $('.allProducts').html('<tr><h2>Sorry We Have Problems :( </h2></tr>');
    } else {
      let thumb = '';
			for (let i in products) { // eslint-disable-line
        thumb += '<tr>';
        thumb += ` <td class="key">${products[i].id} </td>`;
        thumb += ` <td>${products[i].name} </td>`;
        thumb += ` <td>${products[i].status} </td>`;
        thumb += ` <td>${products[i].price}$ </td>`;
        thumb += ` <td>${products[i].total_sales} </td>`;
        thumb += ` <td><img src="${products[i].images[0].src}" height="100" width="100" ></td>`;
        thumb += ' <td>';
        thumb += `  <div class="marginBottom"><a id="${products[i].id}-product" class='open-AddBookDialog btn btn-primary updateProduct'`;
        thumb += `   data-target='#myModalUpdate' data-id="${products[i].id}-product" `;
        thumb += '   data-toggle=\'modal\'>Update</a></div>';
        thumb += `  <div><a id="${products[i].id}-product" class='open-deleteDialog btn btn-danger'`;
        thumb += `   data-target='#myModalDelete' data-id="${products[i].name}" data-toggle='modal'>Delete</a><div>`;
        thumb += ' </td>';
        thumb += '</tr>';
      }
      $('.allProducts').html(thumb);
      $('#totalProducts').html(`${Object.keys(products).length}`);
      $('#totalSales').html(`${sales[0].total_sales}`);
      $('#totalCustomers').html(`${Object.keys(customers).length}`);
      $('#totalOrders').html(`${Object.keys(orders).length}`);
    }
  } else {
    $('.allProducts').html('<tr><h2>Sorry We Have Problems :( </h2></tr>');
  }
}

function startDashBoard() {
  getAllCount();
}

function actionProduct(data, type, cr) {
  let products = ajaxNotAsync('https://consult-wp1.cirel.com.ve/controllers/products-controller.php', {
    type,
    route: `products/${cr}/`,
    data
  });
}

function getFormData(form) {
  let unindexedArray = $(`#${form}`).serializeArray();
  let indexedArray = {};
  let aO = {};
  let image = $(`#${form} #urlImage`).val();
  $.map(unindexedArray, (n, i) => {
    indexedArray[n.name] = n.value;
  });
  aO.src = image;
  aO.position = 0;
  let src = [{
    src: image,
    position: 0
  }];
  indexedArray.images = src;
  return indexedArray;
}


function getProduct(id) {
  let product = ajaxNotAsync('https://consult-wp1.cirel.com.ve/controllers/products-controller.php', {
    type: 'consultAll',
    route: `products/${id}/`,
  });
  $('#formUpdateProduct #name').val(product.name);
  $('#formUpdateProduct #regular_price').val(product.regular_price);
  $('#formUpdateProduct #description').val(product.description);
  $('#formUpdateProduct #short_description').val(product.short_description);
  $('#formUpdateProduct #urlImage').val(product.images[0].src);
  $('#formUpdateProduct #idProduct').val(id);
  $('#formDeleteProduct #idProduct').val(id);
}


$(document).on('ready', () => {
  startDashBoard();
  $('body').on('click', '.updateProduct', (event) => {
    event.preventDefault();
		let id = event.target.id; // eslint-disable-line
    id = id.substring(0, id.indexOf('-'));
    getProduct(id);
  }); // finish click
  $('body').on('submit', '#formRegisterProduct', (event) => {
    event.preventDefault();
    let data = getFormData('formRegisterProduct');
    actionProduct(data, 'insert', '');
    startDashBoard();
    $('#myModal1').modal('toggle');
    $('#formRegisterProduct').trigger('reset');
    // event.currentTarget.submit();
  }); // finish submit
  $('body').on('submit', '#formUpdateProduct', (event) => {
    event.preventDefault();
    let data = getFormData('formUpdateProduct');
    let id = $('#formUpdateProduct #idProduct').val();
    actionProduct(data, 'update', id);
    startDashBoard();
    $('#myModalUpdate').modal('toggle');
    $('#formUpdateProduct').trigger('reset');
  }); // finish click
  $('body').on('submit', '#formDeleteProduct', (event) => {
    event.preventDefault();
    let data = getFormData('formDeleteProduct');
    let id = $('#formDeleteProduct #idProduct').val();
    actionProduct(data, 'delete', id);
    startDashBoard();
    $('#myModalDelete').modal('toggle');
    $('#formDeleteProduct').trigger('reset');
  }); // finish click
});
